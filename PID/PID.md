# PID控制算法

### 1. 控制系统的分类

​	以自动控制方式分类的话，可以分成5类。如下：开环控制系统，闭环控制系统，在线控制系统，在线控制系统，实时控制系统。这边只展示前两种，有兴趣者可以百度了解一下。

#### 1.1 开环控制系统

​	开环控制系统是指一个输出只受系统输入控制的没有反馈回路的系统。如下图：

![开环控制系统](PID_image/开环控制系统流程图.png)

#### 1.2 闭环控制系统

​	闭环控制系统是把控制系统输出量的一部分或全部，通过一定方法和装置反送回系统的输入端，然后将反馈信息与原输入信息进行比较，再将比较的结果施加于系统进行控制，避免系统偏离预定目标。如下图：

![闭环控制系统](PID_image/闭环控制系统.png)

### 2. PID简介

​	PID即：Proportional（比例）、Integral（积分）、Differential（微分）的缩写。顾名思义，PID控制算法是结合比例、积分和微分三种环节于一体的控制算法。公式基本都是教科书上的，其实看看就行，不需要过度理解。下面是连续控制系统的PID公式：

<img src="PID_image/连续PID公式1.png" style="zoom:67%;" />

式中，K~p~——比例增益，K~p~与比例度成倒数关系；

T~t~——积分时间常数；

T~D~——微分时间常数；

u(t)——PID控制器的输出信号；

e(t)——给定值r(t)与测量值之差。

一般PID用于单片机控制，都会简化成离散的公式。如PID==位置式==算法：

![离散PID公式](PID_image/离散PID公式1.png)

然后再把系数进行统一，则有如下式子：

![离散PID公式2](PID_image/离散PID公式2.png)

这样看起来就很好理解了，且这个最终离散化出来的公式将用于代码实现。

还有一种是==增量式==的，就是再位置式的基础上演化的。设 △u(k) = u(k) - u(k-1) ，最终得到的增量式PID的离散公式如下：

![增量式PID](PID_image/增量式PID公式1.png)

### 3. 使用PID控制的目的



以下为PID整体控制的流程图：

![PID控制图](PID_image/PID控制图.png)

我们为什么需要PID呢，比如我控制温度，我就不能监控温度值，温度值一到就停止吗？这种方式（属于开环控制）是肯定可以的，但是对要求高的系统肯定是不可以的，因为温度到了后余热（热惯性）也会让温度继续升高，而且温度自身也会通过空气散热的，这种情况下温度就会波动很大，温度会在我们设定的目标值周围疯狂抖动，很明显这不是我们想要的。我们怎么才能使输出像下图一样快速稳定的到达设定值呢？那肯定是使用PID控制来调节温度。

![温升曲线](PID_image/温升曲线.png)

- P(比例环节)作用：能较快地克服扰动的影响。

​		缺点：产生稳态误差。

​		疑问：何为稳态误差？为什么会产生稳态误差？

​		答：系统达到稳定值的误差。比如一个热水器设定40度通过单比例环节控制（设P=1），刚开始的时候err=40，比例输出肯定很大out=P×err=40，但是当你温度到达38，那么对于比例输出来说就是out=P×err=2；但是热水器他本身还会散热，如果它的散热需要一直输出2来抵消的话，那么此时就达到了一种平衡，即散热速度=加热速度，但是温度此时一直维持在38，这就是稳态误差。因为每个控制系统都会有外界影响因素，所以稳态误差一定会存在。

- I(积分环节)作用：消除稳态误差。

​		缺点：增加超调。

​		疑问：积分为何能消除稳态误差？

​		答：积分项是对累计误差的求和，out=P×err + Σerr，在达到了稳态误差后，由于误差一直存在，此时积分项也会累积增加，P~out~=散热，但是积分项的输出会使温度继续上升，这样就可以消除之前的稳态误差。

- D(微分环节)作用：加大惯性响应速度，减弱超调趋势。

​		疑问：为何能减弱超调？

​		答：微分项是K~d~×(err~i - err~i-1~)，所以在超调之前微分项都是负数，因为误差是越来越小的，所以可以减弱超调的趋势。

- 各个比例系数的作用：

![各个比例系数的作用](PID_image/各个比例系数的作用.png)

### 4. PID调参

- 经验调参法

  按照如下口诀调参就行，先设定I和D为0，调P，然后再调别的。

  “曲线振荡很频繁，比例度盘要放大”说的是比例度过小时，会产生周期较短的激烈振荡，且振荡衰减很慢，严重时至会成为发散振荡。这时就要调大比例度，使曲线平缓下来。

  “曲线漂浮绕大弯，比例度盘往小扳”说的是比例度过大时会使过渡时间过长，使被调参数变化缓慢，即记录曲线偏离给定值幅值较大，时间较长，这时曲线波动较大且变化无规则，形状像绕大弯式的变化，这时就要减小比例度，使余差尽量小。

  “曲线偏离回复慢，积分时间往下降。曲线波动周期长，积分时间再加长”说的是积分作用的整定方法。当积分时间太长时，会使曲线非周期地慢慢地回复到给定值，即“曲线偏离回复慢”，则应减少积分时间。当积分时间太短时，会使曲线振荡周期较长，且衰减很慢，即“曲线波动周期长”，则应加长积分时间。

  ==注意==：它这里的积分时间和我们的K~i~是倒过来的，可以看前面的离散PID公式。![经验调参1](PID_image/经验调参1.png)

- 衰减曲线法

  先调节P，达到第一个波峰和第二个波峰为4：1或者10：1的情况，这时候使用T~s~和T~r~来计算对应的I项和D项。

![衰减曲线法1](PID_image/衰减曲线法1.png)

通过调节P得到比例度，称为n:1衰减比例度δs，两个波峰之间的距离，称为n:1衰减周期Ts。然后对应下表即可查出对应的PID参数。

| 衰减度 | P     | I       | D       |
| ------ | ----- | ------- | ------- |
| 4：1   | δs    | ——      | ——      |
| 4：1   | 1.2δs | 0.5T~s~ | ——      |
| 4：1   | 0.8δs | 0.3T~s~ | 0.1T~s~ |
| 10：1  | δs    | ——      | ——      |
| 10：1  | 1.2δs | 2T~r~   | ——      |
| 10：1  | 0.8δs | 0.3T~r~ | 0.1T~r~ |



### 5. 其他知识

- 积分限幅

积分项不能无穷叠加，代码需要做个限幅机制。

- 积分分离

当误差超过某个值的时候就舍去积分项。即：误差过大时先只用P让误差到达某一个范围以后再用I。

- 微分先行

不使用err给到微分项，而是直接使用反馈值。好处是：当目标值突变时，err也突变，此时不适用err可以消除err突变带来的影响。

### 6. 位置式PID代码实现

这里定义结构体来管理PID参数：

~~~c
typedef struct
{
	float target_val;   //目标值
	float err;          //偏差值
	float err_last;     //上一个偏差值
	float Kp,Ki,Kd;     //比例、积分、微分系数
	float integral;     //积分值
	float output_val;   //输出值
}PID;
~~~

根据离散公式写的简要PID实现函数：

~~~c
float PID_realize(float actual_val)
{
	/*计算目标值与实际值的误差*/
	pid.err = pid.target_val - actual_val;
	
	/*积分项*/
	pid.integral += pid.err;

	/*PID算法实现*/
	pid.output_val = pid.Kp * pid.err + 
				     pid.Ki * pid.integral + 
				     pid.Kd * (pid.err - pid.err_last);
    
	/*误差传递*/
	pid.err_last = pid.err;

	/*返回当前实际值*/
	return pid.output_val;
}
~~~

添加了积分分离和积分限幅的PID实现函数：

~~~c
float PID_Realize(PID *pid, float actual_val)
{
	/*计算目标值与实际值的误差*/
	pid->err = pid->target_val - actual_val;

	/* 设定闭环死区 先略过*/
	if((pid->err >= -LOC_DEAD_ZONE) && (pid->err <= LOC_DEAD_ZONE))
	{
		pid->err = 0;
		pid->integral = 0;
		pid->err_last = 0;
	}

	/*积分项，积分分离，偏差较大时去掉积分作用*/
	if(pid->err > -LOC_INTEGRAL_START_ERR && pid->err < LOC_INTEGRAL_START_ERR)
	{
		pid->integral += pid->err;  
        /*积分范围限定，防止积分饱和*/
		if(pid->integral > LOC_INTEGRAL_MAX_VAL)
		{
			pid->integral = LOC_INTEGRAL_MAX_VAL;
		}
		else if(pid->integral < -LOC_INTEGRAL_MAX_VAL)
		{
			pid->integral = -LOC_INTEGRAL_MAX_VAL;
		}
	}else
    {
    	pid->integral = 0;   
    }	

	/*PID算法实现*/
	pid->output_val = pid->Kp * pid->err +
	                  pid->Ki * pid->integral +
	                  pid->Kd * (pid->err - pid->err_last);

    
    /*最终PID输出限幅*/
    if(pid->output_val > PWM_MAX){
        pid->output_val = PWM_MAX;
    }
    
	/*误差传递*/
	pid->err_last = pid->err;

	/*返回当前实际值*/
	return pid->output_val;
}	
~~~

---

**参考链接：**[**PID原理及代码实现**]([https://blog.csdn.net/name_longming/article/details/115093338?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522166874121216782395331580%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=166874121216782395331580&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-4-115093338-null-null.142^v65^opensearch_v2,201^v3^control_2,213^v2^t3_control2&utm_term=PID%E7%AE%97%E6%B3%95&spm=1018.2226.3001.4187](https://blog.csdn.net/name_longming/article/details/115093338?ops_request_misc=%7B%22request%5Fid%22%3A%22166874121216782395331580%22%2C%22scm%22%3A%2220140713.130102334..%22%7D&request_id=166874121216782395331580&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-4-115093338-null-null.142^v65^opensearch_v2,201^v3^control_2,213^v2^t3_control2&utm_term=PID算法&spm=1018.2226.3001.4187))

END.
